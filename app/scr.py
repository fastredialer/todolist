import os

import django
from django.db import IntegrityError

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "todolist.settings")
django.setup()

# project models must be placed after django setup
from django.contrib.auth.models import User, Group

group = None


def create_group():
    global group
    group, created = Group.objects.get_or_create(name='employee')
    print(f'Group {group}, created: {created}.')


def create_superuser():
    try:
        user = User.objects.create_user(
            username='test',
            email='test@te.com',
            password='test'
        )
        user.is_superuser = True
        user.is_staff = True
        user.save()
    except IntegrityError as e:
        print(f'Provided username already in use. ({e})')


def create_user():
    try:
        user = User.objects.create_user(
            username='test2',
            email='test2@te.com',
            password='test2',
        )
        user.groups.add(group)
    except IntegrityError as e:
        print(f'Provided username already in use. ({e})')


create_group()
create_superuser()
create_user()
