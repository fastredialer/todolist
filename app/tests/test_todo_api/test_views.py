import json

import pytest
from django.contrib.auth.models import User, Group
from django.urls import reverse
from rest_framework.authtoken.models import Token


@pytest.fixture
def test_header():
    return 'first task'


@pytest.fixture
def test_description():
    return 'task description'


@pytest.fixture
def test_username():
    return 'testUsername'


@pytest.fixture
def test_password():
    return 'testPassword'


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient
    return APIClient()


@pytest.fixture
def create_user(db, django_user_model, test_username, test_password):
    def make_user(**kwargs):
        if 'username' not in kwargs:
            kwargs['username'] = test_username
        if 'password' not in kwargs:
            kwargs['password'] = test_password
        user = django_user_model.objects.create_user(**kwargs)
        if kwargs.get('is_superuser'):
            user.is_superuser = True
        user.save()
        return user

    return make_user


@pytest.fixture
def get_or_create_employee_group(db):
    def make_group():
        group, created = Group.objects.get_or_create(name='employee')
        return group

    return make_group


@pytest.fixture
def get_or_create_superuser_token(db, create_user):
    def make_token(**kwargs):
        user = create_user(**kwargs)
        token, _ = Token.objects.get_or_create(user=user)
        return token

    return make_token


@pytest.fixture
def create_task(db, api_client, test_header, test_description):
    def make_task(**kwargs):
        api_client.credentials(HTTP_AUTHORIZATION='Token ' + kwargs['token_key'])
        url = reverse('task-list-view')
        response = api_client.post(url, data={
            'header': kwargs.get('header') or test_header,
            'description': kwargs.get('description') or test_description
        })
        return response

    return make_task


@pytest.mark.django_db
def test_user_creation():
    User.objects.create_user('first_user', 'first_user@test.com', 'strong_password')
    assert User.objects.count() == 1


@pytest.mark.django_db
def test_task_creation(get_or_create_superuser_token, create_task):
    token = get_or_create_superuser_token(username='test', password='test', is_superuser=True)
    response = create_task(token_key=token.key, header='test1', description='test desc')
    assert response.status_code == 201


@pytest.mark.django_db
def test_unauthorized_request(api_client):
    url = reverse('task-list-view')
    response = api_client.get(url)
    assert response.status_code == 401


@pytest.mark.django_db
def test_task_detail(api_client, get_or_create_superuser_token, create_task):
    token = get_or_create_superuser_token(username='test', password='test', is_superuser=True)
    task_header, task_description = 'test1', 'test desc'
    task = create_task(token_key=token.key, header=task_header, description=task_description)
    api_client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
    url = reverse('task-detail-view', kwargs={'pk': task.data['id']})
    response = api_client.get(url)
    assert response.status_code == 200
    assert json.loads(response.content.decode('utf-8'))['header'] == task_header
    assert json.loads(response.content.decode('utf-8'))['description'] == task_description


@pytest.mark.django_db
def test_task_editing_by_author(api_client, get_or_create_superuser_token, create_task):
    token = get_or_create_superuser_token(username='test', password='test', is_superuser=True)
    task = create_task(token_key=token.key, header='test1', description='test desc')
    api_client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
    url = reverse('task-detail-view', kwargs={'pk': task.data['id']})
    new_header = 'new header'
    response = api_client.patch(url, data={'header': new_header})
    assert response.status_code == 200
    assert json.loads(response.content.decode('utf-8'))['header'] == new_header


@pytest.mark.django_db
def test_task_editing_by_another_user(api_client, get_or_create_superuser_token, create_task):
    token1 = get_or_create_superuser_token(username='test', password='test', is_superuser=True)
    token2 = get_or_create_superuser_token(username='test2', password='test2')
    task = create_task(token_key=token1.key, header='test header', description='test desc')
    api_client.credentials(HTTP_AUTHORIZATION='Token ' + token2.key)
    url = reverse('task-detail-view', kwargs={'pk': task.data['id']})
    new_header = 'new header'
    response = api_client.patch(url, data={'header': new_header})
    assert response.status_code == 403
