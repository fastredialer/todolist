from django.contrib import admin

from todo_api.models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    pass
