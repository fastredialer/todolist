from rest_framework import serializers

from todo_api.models import Task


class TaskSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    author_id = serializers.IntegerField(source='author.id', read_only=True)

    class Meta:
        model = Task
        fields = '__all__'
