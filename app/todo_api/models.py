from django.contrib.auth.models import User
from django.db import models


class TrackableDateModel(models.Model):
    """Abstract model to track the create/update date for a model."""
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Task(TrackableDateModel):
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    header = models.CharField(max_length=128)
    description = models.CharField(max_length=512)
    priority = models.PositiveSmallIntegerField(blank=True, null=True)
    deadline = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.header
