from django.urls import path

from todo_api.views import TaskListView, TaskDetailView

urlpatterns = [
    path('tasks/', TaskListView.as_view(), name='task-list-view'),
    path('tasks/<int:pk>/', TaskDetailView.as_view(), name='task-detail-view'),
]
