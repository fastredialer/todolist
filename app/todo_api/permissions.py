from rest_framework import permissions


class TaskListPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        if request.method in permissions.SAFE_METHODS:
            return True
        if user.is_superuser:
            return True
        if user.groups.filter(name='employee').exists():
            return True
        return False


class TaskDetailPermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if request.method in permissions.SAFE_METHODS:
            return True
        if user.is_superuser:
            return True
        if obj.author == user:
            return True
        return False
