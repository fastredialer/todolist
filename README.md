# Todo list Project

## Инструкция по использованию
* ### Склонируйте репозиторий в нужную папку
<code> git clone https://gitlab.com/fastredialer/todolist.git . </code>

* ### Переименуйте файл .env_example и присвойте нужные значения переменным
<code> mv .env_example .env </code>

* ### Запустите docker-compose
<code> sudo docker-compose up --build </code>
